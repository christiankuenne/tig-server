# TIG Stack Server: InfluxDB and Grafana

Docker-compose to run InfluxDB and Grafana. The script creates an initial InfluxDB database, admin login and user login. The database files are mounted to `./volumes/influxdb`. The InfluxDB API runs on port _8086_.

In Grafana, the initial datasource is created automatically ([datasource.yaml](./grafana/provisioning/datasources/datasource.yaml)). The data is mapped to `./volumes/grafana`. Grafana runs on port _3000_.

## Getting Started

Change the dummy passwords in [.env](.env).

Run `docker-compose up` or:

- Start: `make start`
- Stop: `make stop`
- Clear volumes: `sudo make clean`

## TODO

- [HTTPS](https://docs.influxdata.com/influxdb/v1.8/administration/https_setup/)
- Basic Authentication works. Use [JWT Token](https://docs.influxdata.com/influxdb/v1.8/administration/authentication_and_authorization/)?
- Volumes: [permissions issue](https://community.grafana.com/t/new-docker-install-with-persistent-storage-permission-problem/10896)
